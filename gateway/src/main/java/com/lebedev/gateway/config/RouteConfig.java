package com.lebedev.gateway.config;

import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RouteConfig {

    @Bean
    public RouteLocator customRouteLocator(RouteLocatorBuilder builder) {
        return builder.routes()
                .route(r -> r.path("/v1/tours/rural/**")
                        .uri("lb://RURAL-TOUR-SERVICE"))
                .route(r -> r.path("/v1/tours/urban/**")
                        .uri("lb://URBAN-TOUR-SERVICE"))
                .build();

    }
}
