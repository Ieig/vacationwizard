package com.lebedev.urbanserv.validator;

import com.lebedev.urbanserv.annotation.ValidCountry;
import com.lebedev.urbanserv.config.CountryCodeConfig;
import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CountryValidator implements ConstraintValidator<ValidCountry, String> {

    private final CountryCodeConfig countryCodeConfig;

    @Autowired
    public CountryValidator(CountryCodeConfig countryCodeConfig) {
        this.countryCodeConfig = countryCodeConfig;
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        if (value == null || value.trim().isEmpty()) {
            return false;
        }
        return countryCodeConfig.getCountryNameToCodeMap().containsKey(value.toLowerCase());
    }
}
