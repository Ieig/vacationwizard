package com.lebedev.urbanserv.service.impl;

import com.lebedev.urbanserv.model.UrbanTour;
import com.lebedev.urbanserv.repository.UrbanTourRepository;
import com.lebedev.urbanserv.service.CachedPersistenceService;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

@Service
public class CachedPersistenceServiceImpl implements CachedPersistenceService {

    private final UrbanTourRepository urbanTourRepository;

    public CachedPersistenceServiceImpl(UrbanTourRepository urbanTourRepository) {
        this.urbanTourRepository = urbanTourRepository;
    }

    @CachePut(value = "urbanTourCache", key = "#urbanTour.generateCompositeKey()")
    public UrbanTour saveToCacheAndDb(UrbanTour urbanTour) {
        urbanTour.setId(urbanTour.generateCompositeKey());
        urbanTourRepository.save(urbanTour);
        return urbanTour;
    }

    @Cacheable(value = "urbanTourCache", key = "#id")
    public UrbanTour getFromCacheOrDb(String id) {
        return urbanTourRepository.findById(id).orElse(null);
    }
}

