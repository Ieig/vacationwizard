package com.lebedev.urbanserv.service.impl;

import com.lebedev.urbanserv.dto.UrbanTourDto;
import com.lebedev.urbanserv.mapper.TourMapper;
import com.lebedev.urbanserv.model.UrbanTour;
import com.lebedev.urbanserv.service.CachedPersistenceService;
import com.lebedev.urbanserv.service.ScheduleProviderService;
import com.lebedev.urbanserv.service.TourService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TourServiceImpl implements TourService {

    private final CachedPersistenceService cachedPersistenceService;
    private final ScheduleProviderService scheduleProviderService;
    private final TourMapper tourMapper;

    @Autowired
    public TourServiceImpl(CachedPersistenceService cachedPersistenceService, ScheduleProviderService scheduleProviderService, TourMapper tourMapper) {
        this.cachedPersistenceService = cachedPersistenceService;
        this.scheduleProviderService = scheduleProviderService;
        this.tourMapper = tourMapper;
    }

    @Override
    public UrbanTourDto processTour(UrbanTourDto urbanTourDto) {
        // CHECKING CACHE AND PERSISTENCE ONLY FOR GENERIC REQUESTS (NO SPECIAL REQUESTS)
        if (urbanTourDto.getSpecialRequests().isEmpty()) {
            UrbanTour urbanTour = tourMapper.dtoToModel(urbanTourDto);
            urbanTour = cachedPersistenceService.getFromCacheOrDb(urbanTour.generateCompositeKey());
            if (urbanTour.getSchedule() != null) {
                return tourMapper.modelToDto(urbanTour);
            }
        }

        urbanTourDto.setSchedule(scheduleProviderService.provideSchedule(urbanTourDto));

        // CACHING AND PERSISTING ONLY FOR GENERIC REQUESTS (NO SPECIAL REQUESTS)
        // TO BE MADE ASYNC IN THE FUTURE
        if (urbanTourDto.getSpecialRequests().isEmpty()) {
            return tourMapper.modelToDto(
                    cachedPersistenceService.saveToCacheAndDb(
                            tourMapper.dtoToModel(urbanTourDto)));
        }

        return urbanTourDto;
    }
}
