package com.lebedev.urbanserv.service.impl;

import com.lebedev.urbanserv.apiclient.OpenAiClient;
import com.lebedev.urbanserv.dto.UrbanTourDto;
import com.lebedev.urbanserv.enums.InterestType;
import com.lebedev.urbanserv.service.ScheduleProviderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ScheduleProviderServiceImpl implements ScheduleProviderService {

    private final OpenAiClient openAiClient;
    private final String outputFormat;

    @Autowired
    public ScheduleProviderServiceImpl(OpenAiClient openAiClient,
                                       @Value("{app.prompt-main}") String outputFormat) {
        this.openAiClient = openAiClient;
        this.outputFormat = outputFormat;
    }

    @Override
    public List<String> provideSchedule(UrbanTourDto urbanTourDto) {
        String prompt = createPrompt(urbanTourDto);
        String response = openAiClient.createUrbanTourSchedule(prompt);
        return parseResponse(response);
    }

    private String createPrompt(UrbanTourDto urbanTourDto) {
        String prompt = String.format("%s The trip is %d-day and to %s, %s. Budget: %s. Interests: %s.", outputFormat,
                urbanTourDto.getDuration(), urbanTourDto.getCity(), urbanTourDto.getCountry(), urbanTourDto.getBudgetType(),
                urbanTourDto.getInterestTypes().stream().map(InterestType::getType).collect(Collectors.joining(", ")));

        if (urbanTourDto.getSpecialRequests().isEmpty()) {
            return prompt;
        } else {
            return prompt + String.format(" With special requests: %s", urbanTourDto.getSpecialRequests());
        }
    }

    private List<String> parseResponse(String response) {
        return Arrays.stream(response.split("=")).toList();
    }
}
