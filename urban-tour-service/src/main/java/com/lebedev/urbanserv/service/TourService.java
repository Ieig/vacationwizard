package com.lebedev.urbanserv.service;

import com.lebedev.urbanserv.dto.UrbanTourDto;

public interface TourService {

    UrbanTourDto processTour(UrbanTourDto urbanTourDto);
}
