package com.lebedev.urbanserv.service;

import com.lebedev.urbanserv.dto.UrbanTourDto;

import java.util.List;

public interface ScheduleProviderService {

    List<String> provideSchedule(UrbanTourDto urbanTourDto);
}
