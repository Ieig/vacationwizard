package com.lebedev.urbanserv.service;

import com.lebedev.urbanserv.model.UrbanTour;

public interface CachedPersistenceService {

    UrbanTour saveToCacheAndDb(UrbanTour urbanTour);

    UrbanTour getFromCacheOrDb(String id);
}
