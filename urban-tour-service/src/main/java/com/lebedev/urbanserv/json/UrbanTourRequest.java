package com.lebedev.urbanserv.json;

import com.lebedev.urbanserv.annotation.ValidCountry;
import com.lebedev.urbanserv.enums.BudgetType;
import com.lebedev.urbanserv.enums.InterestType;
import jakarta.validation.constraints.*;
import lombok.Data;

import java.util.Set;

@Data
public class UrbanTourRequest {

    @NotEmpty
    private String city;

    @ValidCountry
    private String country;

    @NotNull
    @Min(value = 1)
    @Max(value = 30)
    private Integer duration;

    private BudgetType budgetType;

    @Size(max = 3)
    private Set<InterestType> interestTypes;

    @Size(max = 200)
    private String specialRequests;
}
