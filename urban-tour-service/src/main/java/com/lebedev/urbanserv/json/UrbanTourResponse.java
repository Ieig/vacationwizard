package com.lebedev.urbanserv.json;

import com.lebedev.urbanserv.enums.BudgetType;
import com.lebedev.urbanserv.enums.InterestType;
import lombok.Data;

import java.util.List;
import java.util.Set;

@Data
public class UrbanTourResponse {

    private String city;
    private String country;
    private Integer duration;
    private BudgetType budgetType;
    private Set<InterestType> interestTypes;
    private String specialRequests;
    private List<String> schedule;
}
