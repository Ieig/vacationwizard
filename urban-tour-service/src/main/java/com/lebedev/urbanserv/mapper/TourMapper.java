package com.lebedev.urbanserv.mapper;

import com.lebedev.urbanserv.config.CountryCodeConfig;
import com.lebedev.urbanserv.dto.UrbanTourDto;
import com.lebedev.urbanserv.enums.BudgetType;
import com.lebedev.urbanserv.enums.InterestType;
import com.lebedev.urbanserv.json.UrbanTourRequest;
import com.lebedev.urbanserv.json.UrbanTourResponse;
import com.lebedev.urbanserv.model.UrbanTour;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.Set;
import java.util.TreeSet;

@Mapper(componentModel = "spring", uses = CountryCodeConfig.class)
public interface TourMapper {

    default UrbanTourDto requestToDto(UrbanTourRequest urbanTourRequest) {
        return new UrbanTourDto()
                .setCity(urbanTourRequest.getCity().toLowerCase())
                .setCountry(urbanTourRequest.getCountry().toLowerCase())
                .setDuration(urbanTourRequest.getDuration())
                .setBudgetType(urbanTourRequest.getBudgetType() == null ?
                        BudgetType.FLEXIBLE : urbanTourRequest.getBudgetType())
                .setInterestTypes(urbanTourRequest.getInterestTypes() == null || urbanTourRequest.getInterestTypes().isEmpty() ?
                        Set.of(InterestType.GENERIC) : new TreeSet<>(urbanTourRequest.getInterestTypes()))
                .setSpecialRequests(urbanTourRequest.getSpecialRequests());
    }

    @Mapping(target = "country", source = "country", qualifiedByName = "countryNameToCode")
    @Mapping(target = "id", ignore = true)
    UrbanTour dtoToModel(UrbanTourDto urbanTourDto);

    @Mapping(target = "country", source = "country", qualifiedByName = "countryCodeToName")
    UrbanTourDto modelToDto(UrbanTour urbanTour);

    UrbanTourResponse dtoToResponse(UrbanTourDto urbanTourDto);
}
