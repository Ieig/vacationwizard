package com.lebedev.urbanserv.controller;

import com.lebedev.urbanserv.dto.UrbanTourDto;
import com.lebedev.urbanserv.json.UrbanTourRequest;
import com.lebedev.urbanserv.json.UrbanTourResponse;
import com.lebedev.urbanserv.mapper.TourMapper;
import com.lebedev.urbanserv.service.TourService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.http.ResponseEntity.ok;

@RestController
@RequestMapping("/v1/tours/urban")
public class TourController {

    private final TourService tourService;
    private final TourMapper tourMapper;

    @Autowired
    public TourController(TourService tourService, TourMapper tourMapper) {
        this.tourService = tourService;
        this.tourMapper = tourMapper;
    }

    @PostMapping("/specific")
    public ResponseEntity<UrbanTourResponse> postSpecificTour(@RequestBody @Valid UrbanTourRequest request) {
        UrbanTourDto urbanTourDto = tourMapper.requestToDto(request);

        urbanTourDto = tourService.processTour(urbanTourDto);

        return ok(tourMapper.dtoToResponse(urbanTourDto));
    }
}
