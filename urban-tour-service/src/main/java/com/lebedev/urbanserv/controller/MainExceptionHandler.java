package com.lebedev.urbanserv.controller;

import com.lebedev.urbanserv.json.ErrorResponse;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.ws.rs.InternalServerErrorException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;

@ControllerAdvice
@Slf4j
public class MainExceptionHandler {

    @ExceptionHandler(InternalServerErrorException.class)
    public ResponseEntity<ErrorResponse> errorHandler(HttpServletRequest req, InternalServerErrorException e) {
        log.error("message: {}\n, cause {}\n, cause message: {}", e.getMessage(), e.getCause(), e.getCause().getMessage());
        return new ResponseEntity<>(buildErrorResponse(e, req.getRequestURI(), HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<ErrorResponse> defaultErrorHandler(HttpServletRequest req, Exception e) {
        log.error(Arrays.toString(e.getStackTrace()));
        return new ResponseEntity<>(buildErrorResponse(e, req.getRequestURI(), HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    private ErrorResponse buildErrorResponse(Exception e, String path, HttpStatus status, String message) {
        ErrorResponse errorResponse = new ErrorResponse();
        errorResponse.setException(e.getClass().getSimpleName());
        errorResponse.setStacktrace(Arrays.toString(e.getStackTrace()));
        errorResponse.setStatus(status.value());
        errorResponse.setStatusMessage(status.getReasonPhrase());
        errorResponse.setMessage(message);
        errorResponse.setPath(path);
        errorResponse.setTimestamp(LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd 'at' hh:mm:ss")));
        return errorResponse;
    }
}