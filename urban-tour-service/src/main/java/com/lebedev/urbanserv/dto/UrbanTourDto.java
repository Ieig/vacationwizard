package com.lebedev.urbanserv.dto;

import com.lebedev.urbanserv.enums.BudgetType;
import com.lebedev.urbanserv.enums.InterestType;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;
import java.util.Set;

@Data
@Accessors(chain = true)
public class UrbanTourDto {

    private String city;
    private String country;
    private Integer duration;
    private BudgetType budgetType;
    private Set<InterestType> interestTypes;
    private String specialRequests;
    private List<String> schedule;
}
