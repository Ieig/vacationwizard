package com.lebedev.urbanserv.config;

import jakarta.annotation.PostConstruct;
import lombok.extern.slf4j.Slf4j;
import org.mapstruct.Named;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

@Slf4j
@Configuration
public class CountryCodeConfig {

    private final Map<String, String> countryNameToCodeMap = new HashMap<>();
    private final Map<String, String> countryCodeToNameMap = new HashMap<>();

    @PostConstruct
    public void initCountryMaps() {
        Resource resource = new ClassPathResource("countries.csv");
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(resource.getInputStream()))) {
            String line;
            int lineNumber = 1;
            while ((line = reader.readLine()) != null) {
                String[] parts = line.split(",");
                if (parts.length == 2) {
                    String countryName = parts[0].trim().toLowerCase();
                    String countryCode = parts[1].trim().toUpperCase();
                    countryNameToCodeMap.put(countryName, countryCode);
                    countryCodeToNameMap.put(countryCode, countryName);
                    lineNumber++;
                } else {
                    log.warn("Could not parse the country name/code entry on line: {}, contents is: {}", lineNumber, line);
                }
            }
        } catch (IOException e) {
            throw new IllegalStateException("Failed to load country codes", e);
        }
    }

    public Map<String, String> getCountryNameToCodeMap() {
        return Collections.unmodifiableMap(countryNameToCodeMap);
    }

    public Map<String, String> getCountryCodeToNameMap() {
        return Collections.unmodifiableMap(countryCodeToNameMap);
    }

    @Named("countryNameToCode")
    public String countryNameToCode(String countryName) {
        return countryNameToCodeMap.get(countryName);
    }

    @Named("countryCodeToName")
    public String countryCodeToName(String countryCode) {
        return countryCodeToNameMap.get(countryCode);
    }

}
