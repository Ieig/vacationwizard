package com.lebedev.urbanserv.model;

import com.lebedev.urbanserv.enums.BudgetType;
import com.lebedev.urbanserv.enums.InterestType;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Data
@Document
public class UrbanTour implements Serializable {

    @Id
    private String id;
    private String city;
    private String country;
    private Integer duration;
    private BudgetType budgetType;
    private Set<InterestType> interestTypes;
    private String specialRequests;
    private List<String> schedule;

    public String generateCompositeKey() {
        String interests = interestTypes.stream()
                .map(InterestType::getShortCode)
                .collect(Collectors.joining(",", "[","]"));
        return id = city + "_" + country + '_' + duration + "_" + budgetType.getShortCode() + "_" + interests;
    }
}
