package com.lebedev.urbanserv.apiclient;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(name = "OPEN-AI-SERVICE")
public interface OpenAiClient {

    @PostMapping("/v1/open-ai/completion/urban-tour")
    String createUrbanTourSchedule(@RequestBody String request);
}
