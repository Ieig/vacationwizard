package com.lebedev.urbanserv.repository;

import com.lebedev.urbanserv.model.UrbanTour;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface UrbanTourRepository extends MongoRepository<UrbanTour, String> {
}
