package com.lebedev.openaiserv.json.gpt4;

import lombok.Data;

import java.util.List;

@Data
public class OpenAiGpt4Request {

    private String model;
    private List<Message> messages;
    private Integer max_tokens;
    private Double temperature;
    private Double top_p;
    private Double frequency_penalty;
    private Double presence_penalty;
}
