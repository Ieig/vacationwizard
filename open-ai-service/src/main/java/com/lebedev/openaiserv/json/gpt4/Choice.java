package com.lebedev.openaiserv.json.gpt4;

import lombok.Data;

@Data
public class Choice {

    private Long index;
    private Message message;
    private String finish_reason;
}
