package com.lebedev.openaiserv.json.gpt4;

import lombok.Data;

@Data
public class Usage {

    private Integer prompt_tokens;
    private Integer completion_tokens;
    private Integer total_tokens;
}
