package com.lebedev.openaiserv.json.gpt4;

import lombok.Data;

import java.util.List;

@Data
public class OpenAiGpt4Response {

    private String id;
    private String object;
    private Long created;
    private String model;
    private List<Choice> choices;
    private Usage usage;
    private String system_fingerprint;
}
