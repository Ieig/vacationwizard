package com.lebedev.openaiserv.controller;

import com.lebedev.openaiserv.apiclient.ApiClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static org.springframework.http.ResponseEntity.ok;
import static org.springframework.http.ResponseEntity.status;

@RestController
@RequestMapping("/v1/open-ai/completion/")
public class CompletionController {

    private final ApiClient ApiClient;

    @Autowired
    public CompletionController(ApiClient ApiClient) {
        this.ApiClient = ApiClient;
    }

    @PostMapping("/{caller}")
    public ResponseEntity<String> provideSpecificUrbanTour(@RequestBody String request,
                                                           @PathVariable("caller") String caller) {
        String generatedText = ApiClient.generateText(request, caller);
        if (generatedText != null) {
            return ok(generatedText);
        } else {
            return status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }
}
