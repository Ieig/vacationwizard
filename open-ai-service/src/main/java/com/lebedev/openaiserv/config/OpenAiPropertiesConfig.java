package com.lebedev.openaiserv.config;

import com.lebedev.openaiserv.properties.OpenAiProperties;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

@Configuration
public class OpenAiPropertiesConfig {

    @Bean
    @Qualifier(value = "propertyMap")
    public Map<String, OpenAiProperties> openAiPropertiesMap(List<OpenAiProperties> openAiPropertiesList) {
        return openAiPropertiesList.stream().collect(Collectors.toMap(OpenAiProperties::getName, Function.identity()));
    }
}
