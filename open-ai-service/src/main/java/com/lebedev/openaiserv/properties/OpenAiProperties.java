package com.lebedev.openaiserv.properties;

public interface OpenAiProperties {

    String getName();

    String getUrl();

    String getModel();

    Integer getMaxTokens();

    Double getTemperature();

    Double getTopP();

    Double getFrequencyPenalty();

    Double getPresencePenalty();
}
