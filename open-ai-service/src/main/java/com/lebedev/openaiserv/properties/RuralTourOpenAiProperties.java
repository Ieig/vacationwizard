package com.lebedev.openaiserv.properties;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Getter
@Setter
@Configuration
@ConfigurationProperties(prefix = "open-ai.rural-tour")
public class RuralTourOpenAiProperties implements OpenAiProperties {

    private String name;
    private String model;
    private String url;
    private Integer maxTokens;
    private Double temperature;
    private Double topP;
    private Double frequencyPenalty;
    private Double presencePenalty;
}
