package com.lebedev.openaiserv.apiclient;

public interface ApiClient {

    String generateText(String prompt, String caller);
}
