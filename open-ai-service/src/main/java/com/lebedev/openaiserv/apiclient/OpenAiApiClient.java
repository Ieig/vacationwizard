package com.lebedev.openaiserv.apiclient;

import com.lebedev.openaiserv.json.gpt4.Message;
import com.lebedev.openaiserv.json.gpt4.OpenAiGpt4Request;
import com.lebedev.openaiserv.json.gpt4.OpenAiGpt4Response;
import com.lebedev.openaiserv.properties.OpenAiProperties;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.Map;

@Slf4j
@Service
public class OpenAiApiClient implements ApiClient {

    private final RestTemplate restTemplate;
    private final Map<String, OpenAiProperties> openAiPropertiesMap;
    private final String token;

    @Autowired
    public OpenAiApiClient(RestTemplate restTemplate,
                           @Qualifier("propertyMap") Map<String, OpenAiProperties> openAiPropertiesMap,
                           @Value("${open-ai.token}") String token) {
        this.restTemplate = restTemplate;
        this.openAiPropertiesMap = openAiPropertiesMap;
        this.token = token;
    }

    @Override
    public String generateText(String prompt, String caller) {
        OpenAiProperties properties = openAiPropertiesMap.getOrDefault(caller, openAiPropertiesMap.get("default"));

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("Authorization", "Bearer " + token);

        OpenAiGpt4Request request = new OpenAiGpt4Request();
        request.setMessages(List.of(new Message().setRole("user").setContent(prompt)));
        request.setModel(properties.getModel());
        request.setMax_tokens(properties.getMaxTokens());
        request.setTemperature(properties.getTemperature());
        request.setTop_p(properties.getTopP());
        request.setFrequency_penalty(properties.getFrequencyPenalty());
        request.setPresence_penalty(properties.getPresencePenalty());

        OpenAiGpt4Response response = null;

        try {
            response = restTemplate.postForObject(
                    properties.getUrl(),
                    new HttpEntity<>(request, headers),
                    OpenAiGpt4Response.class
            );

        } catch (HttpClientErrorException | HttpServerErrorException e) {
            log.error("Error while interacting with OpenAi with status code: {}, text: {}, response: {}",
                    e.getStatusCode(), e.getStatusText(), e.getResponseBodyAsString());
        } catch (Exception e) {
            log.error("Exception while interacting with OpenAi message: {}, cause: {}",
                    e.getMessage(), e.getCause().getMessage());
        }

        return response != null && response.getChoices() != null && !response.getChoices().isEmpty()
                ? response.getChoices().get(0).getMessage().getContent()
                : null;
    }
}
