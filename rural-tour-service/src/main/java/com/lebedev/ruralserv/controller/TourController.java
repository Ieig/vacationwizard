package com.lebedev.ruralserv.controller;

import com.lebedev.ruralserv.dto.RuralTourDto;
import com.lebedev.ruralserv.json.RuralTourRequest;
import com.lebedev.ruralserv.json.RuralTourResponse;
import com.lebedev.ruralserv.mapper.TourMapper;
import com.lebedev.ruralserv.service.TourService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.http.ResponseEntity.ok;

@RestController
@RequestMapping("/v1/tours/rural")
public class TourController {

    private final TourService tourService;
    private final TourMapper tourMapper;

    @Autowired
    public TourController(TourService tourService, TourMapper tourMapper) {
        this.tourService = tourService;
        this.tourMapper = tourMapper;
    }

    @PostMapping("/specific")
    public ResponseEntity<RuralTourResponse> postSpecificTour(@RequestBody @Valid RuralTourRequest request) {
        RuralTourDto ruralTourDto = tourMapper.requestToDto(request);

        ruralTourDto = tourService.processTour(ruralTourDto);

        return ok(tourMapper.dtoToResponse(ruralTourDto));
    }
}
