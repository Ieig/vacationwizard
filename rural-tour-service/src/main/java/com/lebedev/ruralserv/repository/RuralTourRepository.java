package com.lebedev.ruralserv.repository;

import com.lebedev.ruralserv.model.RuralTour;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface RuralTourRepository extends MongoRepository<RuralTour, String> {
}
