package com.lebedev.ruralserv.json;

import com.lebedev.ruralserv.enums.BudgetType;
import com.lebedev.ruralserv.enums.InterestType;
import lombok.Data;

import java.util.List;
import java.util.Set;

@Data
public class RuralTourResponse {

    private String startCity;
    private String finishCity;
    private String startCountry;
    private String finishCountry;
    private Integer duration;
    private BudgetType budgetType;
    private Set<InterestType> interestTypes;
    private String specialRequests;
    private List<String> schedule;
}
