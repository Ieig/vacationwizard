package com.lebedev.ruralserv.json;

import com.lebedev.ruralserv.annotation.ValidCountry;
import com.lebedev.ruralserv.enums.BudgetType;
import com.lebedev.ruralserv.enums.InterestType;
import jakarta.validation.constraints.*;
import lombok.Data;

import java.util.Set;

@Data
public class RuralTourRequest {

    @NotEmpty
    private String startCity;

    @NotEmpty
    private String finishCity;

    @ValidCountry
    private String startCountry;

    @ValidCountry
    private String finishCountry;

    @NotNull
    @Min(value = 1)
    @Max(value = 30)
    private Integer duration;

    private BudgetType budgetType;

    @Size(max = 3)
    private Set<InterestType> interestTypes;

    @Size(max = 200)
    private String specialRequests;
}
