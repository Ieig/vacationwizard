package com.lebedev.ruralserv.json;

import lombok.Data;

@Data
public class ErrorResponse {

    private String exception;
    private String stacktrace;
    private Integer status;
    private String statusMessage;
    private String message;
    private String path;
    private String timestamp;
}

