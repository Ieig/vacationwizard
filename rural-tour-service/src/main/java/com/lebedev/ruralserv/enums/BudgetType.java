package com.lebedev.ruralserv.enums;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;

@Getter
public enum BudgetType {

    @JsonProperty("backpacker")
    BACKPACKER("backpacker", "Very low budget, basic accommodations", "B"),

    @JsonProperty("economy")
    ECONOMY("economy", "Minimal expenses, focusing on free or low-cost activities", "E"),

    @JsonProperty("moderate")
    MODERATE("moderate", "Balanced spending with some splurges", "M"),

    @JsonProperty("business")
    BUSINESS("business", "Comfort and efficiency, suitable for business travelers", "BI"),

    @JsonProperty("luxury")
    LUXURY("luxury", "High-end accommodations and exclusive experiences", "L"),

    @JsonProperty("flexible")
    FLEXIBLE("flexible", "Combination of various budget levels", "F");

    private final String type;
    private final String description;
    private final String shortCode;

    BudgetType(String type, String description, String shortCode) {
        this.type = type;
        this.description = description;
        this.shortCode = shortCode;
    }
}
