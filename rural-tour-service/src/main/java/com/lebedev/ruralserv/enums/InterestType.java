package com.lebedev.ruralserv.enums;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;

@Getter
public enum InterestType {

    @JsonProperty("cultural")
    CULTURAL("Cultural", "Museums, historical sites, local traditions, cultural performances", "C"),

    @JsonProperty("nature outdoors")
    NATURE_OUTDOORS("Nature and Outdoors", "National parks, hiking, beaches, wildlife, outdoor adventures", "NO"),

    @JsonProperty("gastronomy")
    GASTRONOMY("Gastronomy", "Local cuisine, food tours, cooking classes, wine tasting, fine dining", "G"),

    @JsonProperty("art music")
    ART_MUSIC("Art and Music", "Art galleries, street art, concerts, music festivals", "AM"),

    @JsonProperty("adventure sports")
    ADVENTURE_SPORTS("Adventure and Sports", "Extreme sports, water sports, cycling, sports events", "AS"),

    @JsonProperty("relaxation")
    RELAXATION("Relaxation", "Spas, wellness centers, leisurely walks, beach relaxation", "R"),

    @JsonProperty("shopping")
    SHOPPING("Shopping", "Markets, shopping districts, local crafts, high-end boutiques", "S"),

    @JsonProperty("education learning")
    EDUCATION_LEARNING("Education and Learning", "Workshops, educational tours, language learning, lectures", "E"),

    @JsonProperty("family friendly")
    FAMILY_FRIENDLY("Family-Friendly", "Activities suitable for children, theme parks, family-friendly attractions", "F"),

    @JsonProperty("nightlife")
    NIGHTLIFE("Nightlife", "Clubs, bars, nightlife tours, evening entertainment", "N"),

    @JsonProperty("generic")
    GENERIC("Generic", "General or undefined interests", "GE");

    private final String type;
    private final String description;
    private final String shortCode;

    InterestType(String type, String description, String shortCode) {
        this.type = type;
        this.description = description;
        this.shortCode = shortCode;
    }
}

