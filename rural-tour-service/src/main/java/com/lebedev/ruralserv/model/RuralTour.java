package com.lebedev.ruralserv.model;

import com.lebedev.ruralserv.enums.BudgetType;
import com.lebedev.ruralserv.enums.InterestType;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Data
@Document
public class RuralTour implements Serializable {

    @Id
    private String id;
    private String startCity;
    private String finishCity;
    private String startCountry;
    private String finishCountry;
    private Integer duration;
    private BudgetType budgetType;
    private Set<InterestType> interestTypes;
    private String specialRequests;
    private List<String> schedule;

    public String generateCompositeKey() {
        String interests = interestTypes.stream()
                .map(InterestType::getShortCode)
                .collect(Collectors.joining(",", "[","]"));
        return id = startCity + "_" +
                startCountry + "_" +
                finishCity + "_" +
                finishCountry + "_" +
                duration + budgetType.getShortCode() + "_" +
                interests;
    }
}
