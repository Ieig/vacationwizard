package com.lebedev.ruralserv.mapper;

import com.lebedev.ruralserv.config.CountryCodeConfig;
import com.lebedev.ruralserv.dto.RuralTourDto;
import com.lebedev.ruralserv.enums.BudgetType;
import com.lebedev.ruralserv.enums.InterestType;
import com.lebedev.ruralserv.json.RuralTourRequest;
import com.lebedev.ruralserv.json.RuralTourResponse;
import com.lebedev.ruralserv.model.RuralTour;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.Set;
import java.util.TreeSet;

@Mapper(componentModel = "spring", uses = CountryCodeConfig.class)
public interface TourMapper {

    default RuralTourDto requestToDto(RuralTourRequest ruralTourRequest) {
        return new RuralTourDto()
                .setStartCity(ruralTourRequest.getStartCity())
                .setFinishCity(ruralTourRequest.getFinishCity())
                .setStartCountry(ruralTourRequest.getStartCountry())
                .setFinishCountry(ruralTourRequest.getFinishCountry())
                .setDuration(ruralTourRequest.getDuration())
                .setBudgetType(ruralTourRequest.getBudgetType() == null ?
                        BudgetType.FLEXIBLE : ruralTourRequest.getBudgetType())
                .setInterestTypes(ruralTourRequest.getInterestTypes() == null || ruralTourRequest.getInterestTypes().isEmpty() ?
                        Set.of(InterestType.GENERIC) : new TreeSet<>(ruralTourRequest.getInterestTypes()))
                .setSpecialRequests(ruralTourRequest.getSpecialRequests());
    }

    @Mapping(target = "startCountry", source = "startCountry", qualifiedByName = "countryNameToCode")
    @Mapping(target = "finishCountry", source = "finishCountry", qualifiedByName = "countryNameToCode")
    @Mapping(target = "id", ignore = true)
    RuralTour dtoToModel(RuralTourDto ruralTourDto);

    @Mapping(target = "startCountry", source = "startCountry", qualifiedByName = "countryCodeToName")
    @Mapping(target = "finishCountry", source = "finishCountry", qualifiedByName = "countryCodeToName")
    RuralTourDto modelToDto(RuralTour ruralTour);

    RuralTourResponse dtoToResponse(RuralTourDto ruralTourDto);
}
