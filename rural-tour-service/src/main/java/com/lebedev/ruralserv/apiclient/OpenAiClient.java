package com.lebedev.ruralserv.apiclient;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(name = "OPEN-AI-SERVICE")
public interface OpenAiClient {

    @PostMapping("/v1/open-ai/completion/rural-tour")
    String createUrbanTourSchedule(@RequestBody String request);
}
