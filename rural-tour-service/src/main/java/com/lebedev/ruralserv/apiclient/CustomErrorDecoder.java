package com.lebedev.ruralserv.apiclient;

import feign.Response;
import feign.codec.ErrorDecoder;
import jakarta.ws.rs.InternalServerErrorException;

public class CustomErrorDecoder implements ErrorDecoder {

    @Override
    public Exception decode(String methodKey, Response response) {
        if (response.status() == 500) {
            return new InternalServerErrorException("Internal Server Error occurred");
        }
        return new Default().decode(methodKey, response);
    }
}
