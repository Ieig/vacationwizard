package com.lebedev.ruralserv.service;

import com.lebedev.ruralserv.model.RuralTour;

public interface CachedPersistenceService {

    RuralTour saveToCacheAndDb(RuralTour ruralTour);

    RuralTour getFromCacheOrDb(String id);
}
