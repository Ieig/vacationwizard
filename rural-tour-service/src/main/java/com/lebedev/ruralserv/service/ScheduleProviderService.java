package com.lebedev.ruralserv.service;

import com.lebedev.ruralserv.dto.RuralTourDto;

import java.util.List;

public interface ScheduleProviderService {

    List<String> provideSchedule(RuralTourDto ruralTourDto);
}
