package com.lebedev.ruralserv.service.impl;

import com.lebedev.ruralserv.apiclient.OpenAiClient;
import com.lebedev.ruralserv.dto.RuralTourDto;
import com.lebedev.ruralserv.enums.InterestType;
import com.lebedev.ruralserv.service.ScheduleProviderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

import static java.util.stream.Collectors.joining;

@Service
public class ScheduleProviderServiceImpl implements ScheduleProviderService {

    private final OpenAiClient openAiClient;
    private final String outputFormat;

    @Autowired
    public ScheduleProviderServiceImpl(OpenAiClient openAiClient,
                                       @Value("{app.prompt-main}") String outputFormat) {
        this.openAiClient = openAiClient;
        this.outputFormat = outputFormat;
    }

    @Override
    public List<String> provideSchedule(RuralTourDto ruralTourDto) {
        String prompt = createPrompt(ruralTourDto);
        String response = openAiClient.createUrbanTourSchedule(prompt);
        return parseResponse(response);
    }

    private String createPrompt(RuralTourDto ruralTourDto) {
        String prompt = String.format("%s The trip is %d-day long, starts in %s, %s and finishes in %s, %s. " +
                        "Budget preference: %s. My interests are: %s.", outputFormat,
                ruralTourDto.getDuration(), ruralTourDto.getStartCity(), ruralTourDto.getStartCountry(),
                ruralTourDto.getFinishCity(), ruralTourDto.getFinishCountry(), ruralTourDto.getBudgetType(),
                ruralTourDto.getInterestTypes().stream().map(InterestType::getType).collect(joining(", ")));

        if (ruralTourDto.getSpecialRequests().isEmpty()) {
            return prompt;
        } else {
            return prompt + String.format(" With special requests: %s", ruralTourDto.getSpecialRequests());
        }
    }

    private List<String> parseResponse(String response) {
        return Arrays.stream(response.split("=")).toList();
    }
}
