package com.lebedev.ruralserv.service.impl;

import com.lebedev.ruralserv.model.RuralTour;
import com.lebedev.ruralserv.repository.RuralTourRepository;
import com.lebedev.ruralserv.service.CachedPersistenceService;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

@Service
public class CachedPersistenceServiceImpl implements CachedPersistenceService {

    private final RuralTourRepository ruralTourRepository;

    public CachedPersistenceServiceImpl(RuralTourRepository ruralTourRepository) {
        this.ruralTourRepository = ruralTourRepository;
    }

    @CachePut(value = "ruralTourCache", key = "#ruralTour.generateCompositeKey()")
    public RuralTour saveToCacheAndDb(RuralTour ruralTour) {
        ruralTour.setId(ruralTour.generateCompositeKey());
        ruralTourRepository.save(ruralTour);
        return ruralTour;
    }

    @Cacheable(value = "ruralTourCache", key = "#id")
    public RuralTour getFromCacheOrDb(String id) {
        return ruralTourRepository.findById(id).orElse(null);
    }
}

