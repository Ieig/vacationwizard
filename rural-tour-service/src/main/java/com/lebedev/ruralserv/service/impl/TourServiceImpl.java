package com.lebedev.ruralserv.service.impl;

import com.lebedev.ruralserv.dto.RuralTourDto;
import com.lebedev.ruralserv.mapper.TourMapper;
import com.lebedev.ruralserv.model.RuralTour;
import com.lebedev.ruralserv.service.CachedPersistenceService;
import com.lebedev.ruralserv.service.ScheduleProviderService;
import com.lebedev.ruralserv.service.TourService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TourServiceImpl implements TourService {

    private final CachedPersistenceService cachedPersistenceService;
    private final ScheduleProviderService scheduleProviderService;
    private final TourMapper tourMapper;

    @Autowired
    public TourServiceImpl(CachedPersistenceService cachedPersistenceService, ScheduleProviderService scheduleProviderService, TourMapper tourMapper) {
        this.cachedPersistenceService = cachedPersistenceService;
        this.scheduleProviderService = scheduleProviderService;
        this.tourMapper = tourMapper;
    }

    @Override
    public RuralTourDto processTour(RuralTourDto ruralTourDto) {
        // CHECKING CACHE AND PERSISTENCE ONLY FOR GENERIC REQUESTS (NO SPECIAL REQUESTS)
        if (ruralTourDto.getSpecialRequests().isEmpty()) {
            RuralTour ruralTour = tourMapper.dtoToModel(ruralTourDto);
            ruralTour = cachedPersistenceService.getFromCacheOrDb(ruralTour.generateCompositeKey());
            if (ruralTour.getSchedule() != null) {
                return tourMapper.modelToDto(ruralTour);
            }
        }

        ruralTourDto.setSchedule(scheduleProviderService.provideSchedule(ruralTourDto));

        // CACHING AND PERSISTING ONLY FOR GENERIC REQUESTS (NO SPECIAL REQUESTS)
        // TO BE MADE ASYNC IN THE FUTURE
        if (ruralTourDto.getSpecialRequests().isEmpty()) {
            return tourMapper.modelToDto(
                    cachedPersistenceService.saveToCacheAndDb(
                            tourMapper.dtoToModel(ruralTourDto)));
        }

        return ruralTourDto;
    }
}
