package com.lebedev.ruralserv.service;

import com.lebedev.ruralserv.dto.RuralTourDto;

public interface TourService {

    RuralTourDto processTour(RuralTourDto ruralTourDto);
}
