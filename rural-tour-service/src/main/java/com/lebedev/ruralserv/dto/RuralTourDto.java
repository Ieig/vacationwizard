package com.lebedev.ruralserv.dto;

import com.lebedev.ruralserv.enums.BudgetType;
import com.lebedev.ruralserv.enums.InterestType;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;
import java.util.Set;

@Data
@Accessors(chain = true)
public class RuralTourDto {

    private String startCity;
    private String finishCity;
    private String startCountry;
    private String finishCountry;
    private Integer duration;
    private BudgetType budgetType;
    private Set<InterestType> interestTypes;
    private String specialRequests;
    private List<String> schedule;
}
